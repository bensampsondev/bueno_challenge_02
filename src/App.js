import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

import Slide1 from './Slide1';
import Slide2 from './Slide2';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/1" component={Slide1} />
          <Route exact path="/2" component={Slide2} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
